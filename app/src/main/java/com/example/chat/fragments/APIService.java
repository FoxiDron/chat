package com.example.chat.fragments;

import com.example.chat.notification.MyResponse;
import com.example.chat.notification.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAHvQ1jp0:APA91bG8K5AnmJtvbVUkjpl7wNzjau9oghWzbb7KAZ41B2UOCTg1On3xoiHuOLcMkVQcGaUVfTfEblAF_v-KsZ3Ssb0BIteGT2x5OWhUeQUDRlLcDPBPEHKGA4kgFMeTejIYwHLYmaGi"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
