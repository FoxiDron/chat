package com.example.chat.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.chat.MessageActivity;
import com.example.chat.R;
import com.example.chat.model.Chat;
import com.example.chat.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import javax.annotation.Nullable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private Context mContext;
    private List<User> mUsers;
    private boolean isChat;

    FirebaseUser firebaseUser;
    FirebaseFirestore db;

    String mLastMsg;

    public UsersAdapter(Context context, List<User> users, boolean isChat) {
        this.mContext = context;
        this.mUsers = users;
        this.isChat = isChat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.user_item, parent, false);
        return new UsersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final User user = mUsers.get(position);
        holder.userName.setText(user.getUsername());
        if (user.getImageURL().equals("default")) {
            holder.userImage.setImageResource(R.drawable.ic_account);
        } else {
            Glide.with(mContext).load(user.getImageURL()).into(holder.userImage);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MessageActivity.class);
                intent.putExtra("userid", user.getId());
                mContext.startActivity(intent);
            }
        });

        if (isChat) {
            lastMessage(user.getId(), holder.lastMsg);
            if (user.getStatus().equals("online")) {
                holder.userStatusOffline.setVisibility(View.GONE);
                holder.userStatusOnline.setVisibility(View.VISIBLE);
            } else {
                holder.userStatusOnline.setVisibility(View.GONE);
                holder.userStatusOffline.setVisibility(View.VISIBLE);
            }
        } else {
            holder.lastMsg.setVisibility(View.GONE);
            holder.userStatusOffline.setVisibility(View.GONE);
            holder.userStatusOnline.setVisibility(View.GONE);
        }
    }

    private void lastMessage(final String userID, final TextView lastMsg) {
        mLastMsg = "default";
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseFirestore.getInstance();

        Query query = db.collection("Chats").orderBy("date");
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                            Chat chat = snapshot.toObject(Chat.class);
                            if (chat.getReceiver().equals(firebaseUser.getUid()) && chat.getSender().equals(userID) ||
                                    chat.getReceiver().equals(userID) && chat.getSender().equals(firebaseUser.getUid())) {
                                mLastMsg = chat.getMessage();
                            }
                        }
                        switch (mLastMsg) {
                            case "default":
                                lastMsg.setText("No Message");
                                break;
                            default:
                                lastMsg.setText(mLastMsg);
                                break;
                        }

                        mLastMsg = "default";
                    }
                });
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView userName;
        TextView lastMsg;
        CircleImageView userImage;
        CircleImageView userStatusOffline;
        CircleImageView userStatusOnline;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            userName = itemView.findViewById(R.id.userItemName);
            lastMsg = itemView.findViewById(R.id.lastMsg);
            userImage = itemView.findViewById(R.id.userItemProfileImage);
            userStatusOffline = itemView.findViewById(R.id.statusOffline);
            userStatusOnline = itemView.findViewById(R.id.statusOnline);
        }
    }
}
