package com.example.chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.chat.adapter.MessageAdapter;
import com.example.chat.fragments.APIService;
import com.example.chat.model.Chat;
import com.example.chat.model.User;
import com.example.chat.notification.Client;
import com.example.chat.notification.Data;
import com.example.chat.notification.MyResponse;
import com.example.chat.notification.Sender;
import com.example.chat.notification.Token;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;

public class MessageActivity extends AppCompatActivity {

    CircleImageView mUserImage;
    TextView mUserName;

    FirebaseUser firebaseUser;
    DocumentReference docRef;
    FirebaseFirestore db;
    ListenerRegistration registration;

    Intent intent;

    ImageButton btnMessageSend;
    EditText messageEditText;

    MessageAdapter messageAdapter;
    List<Chat> mChat;

    RecyclerView recyclerView;

    APIService apiService;

    String userId;

    boolean notify = false;

    private final String TAG = "MessageActivity: ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        Toolbar toolbar = findViewById(R.id.chatToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MessageActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        mUserImage = findViewById(R.id.userImage);
        mUserName = findViewById(R.id.userName);

        messageEditText = findViewById(R.id.msgEditText);
        btnMessageSend = findViewById(R.id.btnSend);

        recyclerView = findViewById(R.id.messageRecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        intent = getIntent();
        userId = intent.getStringExtra("userid");

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseFirestore.getInstance();
        docRef = db.collection("Users").document(userId);

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                User user = documentSnapshot.toObject(User.class);
                mUserName.setText(user.getUsername());
                if (user.getImageURL().equals("default")) {
                    mUserImage.setImageResource(R.drawable.ic_account);
                } else {
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(mUserImage);
                }

                readMessage(firebaseUser.getUid(), userId, user.getImageURL());
            }
        });

        btnMessageSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notify = true;
                String msg = messageEditText.getText().toString();
                if (!msg.equals("")) {
                    sendMessage(firebaseUser.getUid(), userId, msg);
                } else {
                    Toast.makeText(MessageActivity.this, "You can't send empty message", Toast.LENGTH_LONG).show();
                }
                messageEditText.setText("");
            }
        });

        seenMessage(userId);
    }

    private void sendMessage(String sender, final String receiver, String message) {

        Date date = Calendar.getInstance().getTime();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("message", message);
        hashMap.put("date", date);
        hashMap.put("isseen", false);
        db.collection("Chats").add(hashMap);
        hashMap.clear();

        hashMap.put("id", receiver);
        db.collection("Users").document(sender).collection("ChatList").document(receiver).set(hashMap);
        hashMap.clear();

        hashMap.put("id", sender);
        db.collection("Users").document(receiver).collection("ChatList").document(sender).set(hashMap);
        hashMap.clear();

        final String msg = message;

        db.collection("Users").document(firebaseUser.getUid()).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        DocumentSnapshot snapshot = task.getResult();
                        if (notify) {
                            User user = snapshot.toObject(User.class);
                            sendNotification(receiver, user.getUsername(), msg);
                        }
                        notify = false;
                    }
                });
    }

    private void sendNotification(String receiver, final String username, final String message) {
        DocumentReference query = db.collection("Tokens").document(receiver);

        query.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot snapshot = task.getResult();
                Token token = snapshot.toObject(Token.class);
                Data data = new Data(firebaseUser.getUid(), R.drawable.ic_account, username+": "+message,
                        "New Message", userId);

                Sender sender = new Sender(data, token.getToken());

                apiService.sendNotification(sender)
                        .enqueue(new Callback<MyResponse>() {
                            @Override
                            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                if (response.code() == 200) {
                                    if (response.body().success != 1) {
                                        Toast.makeText(MessageActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<MyResponse> call, Throwable t) {

                            }
                        });
            }
        });
    }

    private void seenMessage(final String userID) {
        final Query query = db.collection("Chats");
        registration = query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                    Chat chat = snapshot.toObject(Chat.class);
                    if (chat.getReceiver().equals(firebaseUser.getUid()) && chat.getSender().equals(userID)) {
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("isseen", true);
                        snapshot.getReference().update(hashMap);
                    }
                }
            }
        });
    }

    private void readMessage(final String myID, final String userID, final String imageURL) {
        mChat = new ArrayList<>();

        Query query = db.collection("Chats").orderBy("date");

        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                mChat.clear();
                for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                    Chat chat = snapshot.toObject(Chat.class);
                    if (snapshot != null && snapshot.exists()) {
                        if (chat.getSender().equals(myID) && chat.getReceiver().equals(userID) ||
                                chat.getSender().equals(userID) && chat.getReceiver().equals(myID)) {
                            mChat.add(chat);
                        }
                        messageAdapter = new MessageAdapter(MessageActivity.this, mChat, imageURL);
                        recyclerView.setAdapter(messageAdapter);
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        status("online");
        currentUser(userId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        registration.remove();
        status("offline");
        currentUser("none");
    }

    private void status(String status) {
        docRef = db.collection("Users").document(firebaseUser.getUid());

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);
        docRef.update(hashMap);
    }

    private void currentUser(String userID) {
        SharedPreferences.Editor editor = getSharedPreferences("PREF", MODE_PRIVATE).edit();
        editor.putString("currentuser", userID);
        editor.apply();
    }
}
